import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import controller.AuthenticationManager;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class TestAuthUser {

  private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("lms");
  private EntityManager entityManager = emfactory.createEntityManager();
  private AuthenticationManager authenticationManager = new AuthenticationManager(entityManager);

  @Before
  public void setup() {

  }


  @Test
  public void testAuthUser() throws NoSuchAlgorithmException, InvalidKeySpecException {
    boolean user1 = authenticationManager.authenticateUser("user@gmail.com", "user".toCharArray());
    boolean user2 = authenticationManager.authenticateUser("amk@gmail.com", "amk".toCharArray());
    boolean user3 = authenticationManager
        .authenticateUser("Usukhbayar.Purevdorj@yahoo.com", "Usukh".toCharArray());

    assertTrue(user1);
    assertFalse(user2);
    assertFalse(user3);
  }

}

