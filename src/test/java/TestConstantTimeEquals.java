import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import controller.AuthenticationManager;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class TestConstantTimeEquals {

  private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("lms");
  private EntityManager entityManager = emfactory.createEntityManager();
  private AuthenticationManager authenticationManager = new AuthenticationManager(entityManager);

  @Before
  public void setup() {

  }

  @Test
  public void testConstantTimeEquals() throws NoSuchAlgorithmException, InvalidKeySpecException {
    byte[] byte1 = "Any String you want".getBytes();
    byte[] byte2 = "Any String you wans".getBytes();
    byte[] byte3 = "Any String you wants".getBytes();

    boolean user1 = authenticationManager.constantTimeEquals(byte1, byte1);
    boolean user2 = authenticationManager.constantTimeEquals(byte1, byte2);
    boolean user3 = authenticationManager.constantTimeEquals(byte1, byte3);

    assertTrue(user1);
    assertFalse(user2);
    assertFalse(user3);
  }

}

