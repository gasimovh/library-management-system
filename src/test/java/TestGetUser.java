import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import controller.AuthenticationManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class TestGetUser {

  private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("lms");
  private EntityManager entityManager = emfactory.createEntityManager();
  private AuthenticationManager authenticationManager = new AuthenticationManager(entityManager);

  @Before
  public void setup() {

  }

  @Test
  public void testGetUser() {
    boolean user1 = authenticationManager.usernameUser("user@gmail.com");
    boolean user2 = authenticationManager.usernameUser("Usukhbayar.Purevdorj@yahoo.com");

    assertTrue(user1);
    assertFalse(user2);
  }

}

