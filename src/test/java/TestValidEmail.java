import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import controller.AuthenticationManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.jupiter.api.Test;


class TestValidEmail {

  private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("lms");
  private EntityManager entityManager = emfactory.createEntityManager();
  private AuthenticationManager authenticationManager = new AuthenticationManager(entityManager);

  @Before
  public void setup() {

  }

  @Test
  public void testValidEmail() {
    boolean isValidEmail = authenticationManager.validEmail("a@yaho0.com");
    boolean isValidEmail1 = authenticationManager.validEmail("ayaho0.com");

    assertTrue(isValidEmail);
    assertFalse(isValidEmail1);
  }

}

