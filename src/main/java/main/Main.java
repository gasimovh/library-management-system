package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        try{
            //FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserPage.fxml"));
            //loader.setController(new UserPageController());
            AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/view/UserLogin.fxml"));
            //Parent root = loader.load();

            Scene scene = new Scene(root);

            scene.getStylesheets().add(getClass().getResource("/view/style.css").toExternalForm());
            stage.setTitle("Library App");
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    public static void main(String[] args) {
        launch(args);
    }
}
