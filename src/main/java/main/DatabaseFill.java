package main;

import controller.AuthenticationManager;
import model.Admin;
import model.Book;
import model.Rent;
import model.User;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;

public class DatabaseFill {

    public static void main(String [] args) throws InvalidKeySpecException, NoSuchAlgorithmException {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("lms");
        EntityManager entityManager = emfactory.createEntityManager();
        entityManager.getTransaction().begin();
        AuthenticationManager auth = new AuthenticationManager(entityManager);

        //AuthenticationManager auth = new AuthenticationManager(entityManager);

        //User user1 = auth.registerUser("Daniel", "dani", "daniel96@gmail.com", "123".toCharArray());
        //entityManager.persist(user1);

        byte [] salt = new byte[32];
        new Random().nextBytes(salt);
        User user1 = new User("Huseyn", hashPassword("user".toCharArray(), salt), salt, "user@gmail.com" );
        entityManager.persist(user1);

        Admin admin1 = new Admin("Spy", hashPassword("admin".toCharArray(), salt), salt, "admin");
        entityManager.persist(admin1);


        Book book1 = new Book("Post Office", "Charles Bukowski");
        Book book2 = new Book("Women", "Charles Bukowski");
        Book book3 = new Book("Pulp", "Charles Bukowski");

        entityManager.persist(book1);
        entityManager.persist(book2);
        entityManager.persist(book3);

//        Rent rent1 = new Rent();
//        Rent rent2 = new Rent();
//        Rent rent3 = new Rent();
//        Rent rent4 = new Rent();
//
//        entityManager.persist(rent1);
//        entityManager.persist(rent2);
//        entityManager.persist(rent3);
//        entityManager.persist(rent4);


        entityManager.getTransaction().commit();
        entityManager.close();
        emfactory.close();


    }

    private static byte[] hashPassword(char[] password, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeySpec spec = new PBEKeySpec(password, salt, 65536, 128);
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return f.generateSecret(spec).getEncoded();
    }

    private static Date date(String day, String clock) {
        return Date.from(Instant.parse(day + "T" + clock + ".00Z").atZone(ZoneId.systemDefault()).toInstant());
    }
}
