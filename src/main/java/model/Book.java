package model;

import javax.persistence.*;

@Entity
public class Book{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(mappedBy = "book")
    private Rent rent;

    private String title;
    private String author;

    public Book() {}

    public Book(String title,String author) {
        this.title=title;
        this.author=author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() { return author;}

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }
}