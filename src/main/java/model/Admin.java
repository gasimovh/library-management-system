package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Admin{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private byte[] password;
    private byte[] salt;
    private String email;
    private String name;


    public Admin() {}

    public Admin(String name, byte[] password, byte[] salt, String email) {
        this.name = name;
        this.password=password;
        this.salt = salt;
        this.email=email;
    }

    public String getName() {
        return name;
    }

    public byte[] getPassword() {
        return password;
    }
    public byte[] getSalt() {
        return salt;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
}