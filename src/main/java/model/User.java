package model;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private byte [] password;
    private byte [] salt;
    private String email;
    private String name;
    private String picture;

    @OneToMany(cascade = CascadeType.ALL)
    ArrayList<Rent> rents =new ArrayList<Rent>();

    public User() {}

    public User(String name, byte[] password, byte[] salt, String email) {
        this.name = name;
        this.password=password;
        this.salt = salt;
        this.email=email;
    }

    public String getName() {
        return name;
    }

    public byte[] getSalt() {
        return salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() { return id;}

    public ArrayList<Rent> getRents() {
        return rents;
    }

    public void setRents(ArrayList<Rent> rents) {
        this.rents = rents;
    }

    public void addRent(Rent rent) {
        this.rents.add(rent);
    }

    public void removeRent(Rent rent) {
        this.rents.remove(rent);
    }

    public String getPicture(){
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public byte[] getPassword(){
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }


}
