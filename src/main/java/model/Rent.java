package model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    private Book book;

    private double rentPrice;

    @ManyToOne
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dueDate;

    private double penalty;


    public Rent(Book book, double rentPrice, Date issueDate, Date dueDate, double penalty) {
        this.book = book;
        this.rentPrice = rentPrice;
        this.issueDate = issueDate;
        this.dueDate = dueDate;
        this.penalty = penalty;
    }

    public Rent(Book book, Date issueDate, Date dueDate) {
        this.book = book;
        this.issueDate = issueDate;
        this.dueDate = dueDate;
    }

    public Rent() {
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public double getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(double rentPrice) {
        this.rentPrice = rentPrice;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }

    public int getId() {
        return id;
    }
}
