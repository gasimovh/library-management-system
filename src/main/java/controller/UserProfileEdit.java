package controller;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.prefs.Preferences;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.EntityManager;

import org.controlsfx.control.Notifications;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import model.User;

public class UserProfileEdit implements Initializable {

	private EntityManager entityManager = Context.getInstance().getEntityManager();
	private AuthenticationManager auth = new AuthenticationManager(entityManager);
	private Preferences userPreferences = Preferences.userRoot().node(UserLogin.class.getName());

	@FXML
	private AnchorPane content;

	@FXML
	private Label username;

	@FXML
	private JFXTextField email;

	@FXML
	private JFXPasswordField password;

	@FXML
	private Circle picture;

	@FXML
	private FontAwesomeIconView editPhoto;

	private User user;

	@Override
	public void initialize(java.net.URL arg0, ResourceBundle arg1) {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));

		edit();

		String username1 = userPreferences.get("username", null);
		setTextLabel(username, "@" + username1);


	}

	private void setTextLabel(Label label, String username1) {
		label.setText(username1);
	}

	private void setImage(String imagePath) {
		File file = new File(imagePath);
		Image image = new Image(file.toURI().toString());
		picture.setFill(new ImagePattern(image));
	}

	private void edit() {
		editPhoto.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				fileOpener();
			}
		});
	}

	private void fileOpener() {
		FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {
			String filename = selectedFile.getPath();
			user.setPicture(filename);
			File file = new File(filename);
			Image image = new Image(file.toURI().toString());
			picture.setFill(new ImagePattern(image));
		}
	}

	@FXML
	private void done() {
		if (!password.getText().isEmpty() && !email.getText().isEmpty()) {
			if (!auth.validEmail(email.getText())) {
				Notifications.create().title("Warning").text("Give a valid email").graphic(null)
						.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
			} else {
				user.setEmail(email.getText());
				try {
					user.setPassword(hash(password.getText().toCharArray(), user.getSalt()));
				} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				entityManager.getTransaction().begin();
				entityManager.persist(user);
				entityManager.getTransaction().commit();
				Notifications.create().title("Success").text("Profile successfully changed").graphic(null)
						.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
				content.getChildren().clear();
				try {
					content.getChildren().add(FXMLLoader.load(getClass().getResource("/view/UserProfile.fxml")));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			Notifications.create().title("Warning").text("Fill all the data or give valid data").graphic(null)
					.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
		}
	}

	private byte[] hash(char[] password, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
		KeySpec spec = new PBEKeySpec(password, salt, 65536, 128);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		return f.generateSecret(spec).getEncoded();
	}

}