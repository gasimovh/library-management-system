package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.User;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register implements Initializable {

//	private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("WrestlingApp");
//	private EntityManager entityManager = emfactory.createEntityManager();

	@FXML
	private TextField username;

	@FXML
	private PasswordField password;

	@FXML
	private TextField email;

	private AuthenticationManager auth;
	private EntityManager entityManager1;

	@Override
	public void initialize(java.net.URL arg0, ResourceBundle arg1) {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));
		entityManager1 = Context.getInstance().getEntityManager();
		auth = new AuthenticationManager(entityManager1);
	}
	
	@FXML
	public void register() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
		String username1 = username.getText();
		String password1 = password.getText();
		String email1 = email.getText();

		if (!auth.usernameUser(email1)) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Username incorrect");
			alert.setHeaderText(null);
			alert.setContentText("There is already exist user whose username is " + email1);
			alert.showAndWait();
		} else {
			if (!isValidEmailAddress(email1)) {
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Email incorrect");
				alert.setHeaderText(null);
				alert.setContentText("Give valid email");
				alert.showAndWait();
			} else {
				byte[] salt = new byte[32];
				new Random().nextBytes(salt);

				User user = new User(username1, hash(password1.toCharArray(), salt), salt,
						email1);
				entityManager1.getTransaction().begin();
				entityManager1.persist(user);
				entityManager1.getTransaction().commit();
				try {
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/UserLogin.fxml"));
					AnchorPane root1 = fxmlLoader.load();
					Stage stage = new Stage();
					stage.setScene(new Scene(root1));
					stage.show();
					closeButtonAction();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@FXML
    private void adminSignIn() throws IOException {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/AdminLogin.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void userSignIn() throws IOException {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/UserLogin.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage1 = new Stage();
			stage1.setScene(new Scene(root1));
			stage1.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	@FXML
//	private void signUp() throws IOException {
//		try {
//			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/Register.fxml"));
//			AnchorPane root1 = fxmlLoader.load();
//
//			Stage stage1 = new Stage();
//			stage1.setScene(new Scene(root1));
//			stage1.show();
//			closeButtonAction();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	private void closeButtonAction() {
		Stage stage = (Stage) username.getScene().getWindow();
		stage.close();
	}

	public boolean isValidEmailAddress(String emailAddress) {
		String emailRegEx;
		Pattern pattern;
		emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
		pattern = Pattern.compile(emailRegEx);
		Matcher matcher = pattern.matcher(emailAddress);
		if (!matcher.find()) {
			return false;
		}
		return true;
	}

	private static byte[] hash(char[] password, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
		KeySpec spec = new PBEKeySpec(password, salt, 65536, 128);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		return f.generateSecret(spec).getEncoded();
	}
}