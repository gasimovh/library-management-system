package controller;


import com.sun.javafx.binding.ObjectConstant;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.GridPane;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import model.Rent;
import model.User;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

@SuppressWarnings("restriction")
public class IssueReturnController implements Initializable {

    @FXML
    private GridPane content;

    @FXML
    private TableView<Rent> tableView;

    @FXML
    private TableColumn<Rent, String> titleColumn;

    @FXML
    private TableColumn<Rent, String> authorColumn;

    @FXML
    private TableColumn<Rent, String> issueDateColumn;

    @FXML
    private TableColumn<Rent, String> dueDateColumn;

    @FXML
    private TableColumn<Rent, String> penaltyColumn;

    private EntityManager em = Context.getInstance().getEntityManager();
    private AuthenticationManager auth = new AuthenticationManager(em);
    private Preferences userPreferences = Preferences.userRoot().node(this.getClass().getName());
    private Preferences userPreferencesMainPage = Preferences.userRoot().node(UserLogin.class.getName());
    private User user;

    public void initialize(URL location, ResourceBundle resources) {
        String email = userPreferencesMainPage.get("username", null);
        user = auth.getUser(email);


        List<Rent> allRents = auth.allRents();
        tableView.setItems(FXCollections.observableArrayList(allRents));
        settingColumnContents();
        tableView.setRowFactory(tv -> {
            TableRow<Rent> row = settingRowColor();
            settingRowOnClick(row);
            return row;
        });
    }

    TableRow<Rent> settingRowColor() {
        TableRow<Rent> row = new TableRow<Rent>() {
            @Override
            public void updateItem(Rent item, boolean empty) {
                super.updateItem(item, empty);
            }
        };
        return row;
    }

    void settingRowOnClick(TableRow<Rent> row) {
        row.setOnMouseClicked(event -> {
            Rent rent = row.getItem();
            Alert alert = dialog();
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {

                user.removeRent(rent);
                //rent.setBook(null);
                auth.deleteRent(rent);

                em.getTransaction().begin();
                em.persist(user);
                em.persist(rent);
                em.getTransaction().commit();
                content.getChildren().clear();
                try {
                    content.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/issueReturn.fxml")));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                alert.close();
            }
        });
    }

    public void settingColumnContents() {

        titleColumn.setCellValueFactory(new Callback<CellDataFeatures<Rent, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(CellDataFeatures<Rent, String> data) {
                return ObjectConstant.<String>valueOf(data.getValue().getBook().getTitle());
            }

        });

        authorColumn.setCellValueFactory(new Callback<CellDataFeatures<Rent, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(CellDataFeatures<Rent, String> data) {
                return ObjectConstant.<String>valueOf(data.getValue().getBook().getAuthor());
            }

        });

        issueDateColumn.setCellValueFactory(new Callback<CellDataFeatures<Rent, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(CellDataFeatures<Rent, String> data) {
                return ObjectConstant.<String>valueOf(data.getValue().getIssueDate().toString());
            }

        });

        dueDateColumn.setCellValueFactory(new Callback<CellDataFeatures<Rent, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(CellDataFeatures<Rent, String> data) {
                return ObjectConstant.valueOf(data.getValue().getDueDate().toString());
            }

        });

        penaltyColumn.setCellValueFactory(new Callback<CellDataFeatures<Rent, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(CellDataFeatures<Rent, String> data) {
                return ObjectConstant.valueOf(Double.toString(data.getValue().getPenalty()));
            }

        });

    }

    public Alert dialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "You are going to return this book. Are you sure?",
                ButtonType.YES, ButtonType.NO);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setGraphic(null);
        DialogPane dialogPane = alert.getDialogPane();
        //dialogPane.getStylesheets().add(getClass().getResource("/view/appDialogs.css").toExternalForm());
        //dialogPane.getStyleClass().add("myDialog");
        return alert;
    }
}
