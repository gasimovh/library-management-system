package controller;

import com.sun.javafx.binding.ObjectConstant;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import model.User;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

@SuppressWarnings("restriction")
public class UserListController implements Initializable {

	private EntityManager em = Context.getInstance().getEntityManager();
	private AuthenticationManager auth = new AuthenticationManager(em);
	private Preferences userPreferences = Preferences.userRoot().node(this.getClass().getName());
	private Preferences userPreferencesMainPage = Preferences.userRoot().node(UserLogin.class.getName());

	@FXML
	private Pane content;

	@FXML
	private TableView<User> tableView;

	@FXML
	private TableColumn<User, String> username;

	@FXML
	private TableColumn<User, String> email;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		List<User> allUser = auth.allUsers();
		tableView.setItems(FXCollections.observableArrayList(allUser));
		settingColumnContents();
		tableView.setRowFactory(tv -> {
			TableRow<User> row = settingRowColor();
			return row;
		});

	}

	TableRow<User> settingRowColor() {
		TableRow<User> row = new TableRow<User>() {
			@Override
			public void updateItem(User item, boolean empty) {
				super.updateItem(item, empty);
			}
		};
		return row;
	}

	void settingColumnContents() {
		username.setCellValueFactory(new Callback<CellDataFeatures<User, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<User, String> data) {
				return ObjectConstant.<String>valueOf(data.getValue().getName());
			}
		});

		email.setCellValueFactory(new Callback<CellDataFeatures<User, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<User, String> data) {
				return ObjectConstant.<String>valueOf(data.getValue().getEmail());
			}
		});

	}

}
