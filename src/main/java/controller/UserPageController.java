package controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.TimeZone;

public class UserPageController implements Initializable {

    @FXML
    private MaterialDesignIconView bell;//notification bell icon

    @FXML
    private MaterialDesignIconView account;//user account icon

    @FXML
    private Pane pane;//to store each related page when it is selected

    @FXML
    private Label aboutLabel;

    @FXML
    private Button aboutBtn;

    public void initialize(URL location, ResourceBundle resources) {
        //set label invisible
        //aboutLabel.setVisible(false);
        //aboutSelected();

        booksSelected();
        account();

    }

    public void aboutSelected()
    {
        //shows the about
//        aboutBtn.focusedProperty().addListener(new ChangeListener<Boolean>() {
//            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                if(newValue){
//                    aboutLabel.setVisible(true);
//                }else {
//                    aboutLabel.setVisible(false);
//                }
//            }
//        });
    }

    @FXML
    public void booksSelected() {
        pane.getChildren().clear();
        try {
            pane.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/BookList.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void dashboardSelected() {
        pane.getChildren().clear();
        try {
            pane.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/Dashboard.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void issueAndReturnSelected(ActionEvent event) {
        pane.getChildren().clear();
        try {
            pane.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/IssueReturn.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void logoutSelected(ActionEvent event) {
        ((Stage) pane.getScene().getWindow()).close();
        try{
            //FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserPage.fxml"));
            //loader.setController(new UserPageController());
            AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/view/UserLogin.fxml"));
            //Parent root = loader.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            scene.getStylesheets().add(getClass().getResource("/view/style.css").toExternalForm());
            stage.setTitle("Library App");
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void loadPage(String path){
        pane.getChildren().clear();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            Parent root = loader.load();
            pane.getChildren().add(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void account(){
        account.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                pane.getChildren().clear();
                try {
                    pane.getChildren().add(FXMLLoader.load(getClass().getResource("/view/UserProfile.fxml")));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }


}
