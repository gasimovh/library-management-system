package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.prefs.Preferences;

import javax.persistence.EntityManager;

import javafx.scene.control.TextField;
import org.controlsfx.control.Notifications;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import model.Book;

public class AdminBookEditController implements Initializable {

	private EntityManager entityManager = Context.getInstance().getEntityManager();
	private AuthenticationManager auth = new AuthenticationManager(entityManager);
	private Preferences userPreferences = Preferences.userRoot().node(AdminBookListController.class.getName());

	@FXML
	private AnchorPane content;

	@FXML
	private TextField title;

	@FXML
	private TextField author;

	private Book book;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));
		int id = Integer.parseInt(userPreferences.get("ID", null));
		book = auth.getBook(id);

		title.setText(book.getTitle());
		author.setText(book.getAuthor());

	}

	@FXML
	private void save() {
		if (!title.getText().isEmpty() && !author.getText().isEmpty()) {
			book.setTitle(title.getText());
			book.setAuthor(author.getText());

			entityManager.getTransaction().begin();
			entityManager.persist(book);
			entityManager.getTransaction().commit();

			content.getChildren().clear();

			Notifications.create().title("Success").text("Book edited").graphic(null)
					.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
			try {
				content.getChildren().add(FXMLLoader.load(getClass().getResource("/view/AdminBookList.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Notifications.create().title("Warning").text("Fill all the fileds").graphic(null)
					.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
		}
	}
}
