package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminPageController implements Initializable {

    @FXML
    private Pane pane;//to store each related page when it is selected

    @FXML
    private Label aboutLabel;

    @FXML
    private Button aboutBtn;

    public void initialize(URL location, ResourceBundle resources) {
        //set label invisible
        //aboutLabel.setVisible(false);
        //aboutSelected();



    }

    public void aboutSelected()
    {
        //shows the about
//        aboutBtn.focusedProperty().addListener(new ChangeListener<Boolean>() {
//            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                if(newValue){
//                    aboutLabel.setVisible(true);
//                }else {
//                    aboutLabel.setVisible(false);
//                }
//            }
//        });
    }

    @FXML
    public void booksSelected(ActionEvent event) {
        pane.getChildren().clear();
        try {
            pane.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/AdminBookList.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void usersSelected(ActionEvent event) {
        pane.getChildren().clear();
        try {
            pane.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/UserList.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void logoutSelected(ActionEvent event) {
        ((Stage) pane.getScene().getWindow()).close();
        try{
            //FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserPage.fxml"));
            //loader.setController(new UserPageController());
            AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/view/UserLogin.fxml"));
            //Parent root = loader.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            scene.getStylesheets().add(getClass().getResource("/view/style.css").toExternalForm());
            stage.setTitle("Library App");
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void loadPage(String path){
        pane.getChildren().clear();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            Parent root = loader.load();
            pane.getChildren().add(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    
}
