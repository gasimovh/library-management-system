package controller;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.prefs.Preferences;

import javax.persistence.EntityManager;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import model.User;

public class UserProfile implements Initializable {

	private EntityManager entityManager = Context.getInstance().getEntityManager();
	private AuthenticationManager auth = new AuthenticationManager(entityManager);
	private Preferences userPreferences = Preferences.userRoot().node(UserLogin.class.getName());

	@FXML
	private AnchorPane content;

	@FXML
	private Label username;

	@FXML
	private Label email;

	@FXML
	private Circle picture;

	@Override
	public void initialize(java.net.URL arg0, ResourceBundle arg1) {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));
		String username1 = userPreferences.get("username", null);
		User user = auth.getUser(username1);
		username.setText(user.getName());
		email.setText(username1);

		String filename = user.getPicture();
		if (filename != null) {
			setImage(filename);
		}

		setTextLabel(email, user.getEmail());

	}

	private void setTextLabel(Label label, String username1) {
		label.setText(username1);
	}

	private void setImage(String imagePath) {
		File file = new File(imagePath);
		Image image = new Image(file.toURI().toString());
		picture.setFill(new ImagePattern(image));
	}

	@FXML
	private void edit() {
		content.getChildren().clear();
		try {
			content.getChildren().add(FXMLLoader.load(getClass().getResource("/view/UserProfileEdit.fxml")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}