package controller;


import model.*;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthenticationManager {

    private static final SecureRandom RAND = new SecureRandom();

    private final EntityManager em;
    private final CriteriaBuilder cb;
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);


    public AuthenticationManager(EntityManager em) {
        this.em = em;
        this.cb =  em.getCriteriaBuilder();
    }

    private byte[] hashPassword(char[] password, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeySpec spec = new PBEKeySpec(password, salt, 65536, 128);
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return f.generateSecret(spec).getEncoded();
    }

    public boolean constantTimeEquals(byte[] a, byte[] b) {
        int result = 0;
        if (a.length != b.length) {

            System.out.println(a.length + " " + b.length + "s");
            return false;

        }

        for (int i = 0; i < a.length; i++) {
            result |= a[i] ^ b[i];
        }
        return result == 0;
    }


    public boolean authenticateUser(String username, char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
        Root<User> user = userQuery.from(User.class);
        userQuery.where(cb.equal(user.get(User_.email), username));
        userQuery.select(user);

        List<User> us = em.createQuery(userQuery).getResultList();
        if (us.size() == 0) {
            //usernameUserIncorrect(username);
            System.out.println("There is no user whose name is " + username);
            return false;
        }
        User u = us.get(0);
        byte[] hash = hashPassword(password, u.getSalt());
        if (!constantTimeEquals(hash, u.getPassword())) {
            //passwordIncorrect();
            System.out.println("Wrong password");
            return false;
        }

        return true;
    }

    public boolean authenticateAdmin(String username, char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Admin> userQuery = cb.createQuery(Admin.class);
        Root<Admin> user = userQuery.from(Admin.class);
        userQuery.where(cb.equal(user.get(Admin_.email), username));
        userQuery.select(user);

        List<Admin> us = em.createQuery(userQuery).getResultList();
        if (us.size() == 0) {
            //usernameAdminIncorrect(username);
            System.out.println("There is no admin whose name is " + username);
            return false;
        }
        Admin u = us.get(0);

        byte[] hash = hashPassword(password, u.getSalt());
        if (!constantTimeEquals(hash, u.getPassword())) {
            //passwordIncorrect();
            System.out.println("Wrong password");
            return false;
        }
        return true;
    }

    public boolean usernameUser(String username) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
        Root<User> user = userQuery.from(User.class);
        userQuery.where(cb.equal(user.get(User_.email), username));
        userQuery.select(user);

        List<User> us = em.createQuery(userQuery).getResultList();
        if (us.size() == 0) {
            return false;
        }
        return true;
    }

    public User getUser(String username) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
        Root<User> user = userQuery.from(User.class);
        userQuery.where(cb.equal(user.get(User_.email), username));
        userQuery.select(user);
        return em.createQuery(userQuery).getSingleResult();
    }

    public Admin getAdmin(String username) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Admin> userQuery = cb.createQuery(Admin.class);
        Root<Admin> user = userQuery.from(Admin.class);
        userQuery.where(cb.equal(user.get(Admin_.email), username));
        userQuery.select(user);
        return em.createQuery(userQuery).getSingleResult();
    }

    public List<User> allUsers() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
        Root<User> user = userQuery.from(User.class);
        return em.createQuery(userQuery).getResultList();
    }

    public Book getBook(int id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> bookQuery = cb.createQuery(Book.class);
        Root<Book> book = bookQuery.from(Book.class);
        bookQuery.where(cb.equal(book.get(Book_.id), id));
        bookQuery.select(book);
        return em.createQuery(bookQuery).getSingleResult();
    }


    public List<Book> allBooks() {
        CriteriaQuery<Book> allNews = cb.createQuery(Book.class);
        Root<Book> news = allNews.from(Book.class);
        allNews.select(news);
        return em.createQuery(allNews).getResultList();
    }

    public List<Rent> allRents() {
        CriteriaQuery<Rent> allRent = cb.createQuery(Rent.class);
        Root<Rent> rent = allRent.from(Rent.class);
        allRent.select(rent);
        return em.createQuery(allRent).getResultList();
    }

    public void deleteRent(Rent rent){
        CriteriaDelete<Rent> cdelete = cb.createCriteriaDelete(Rent.class);
        Root<Rent> root = cdelete.from(Rent.class);
        cdelete.where(cb.equal(root, rent));
        em.createQuery(cdelete).executeUpdate();
    }

    public boolean validEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

}