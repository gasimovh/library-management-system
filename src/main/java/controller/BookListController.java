package controller;

import com.sun.javafx.binding.ObjectConstant;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import model.Book;
import model.Rent;
import model.User;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

@SuppressWarnings("restriction")
public class BookListController implements Initializable {

	private EntityManager em = Context.getInstance().getEntityManager();
	private AuthenticationManager auth = new AuthenticationManager(em);
	private Preferences userPreferences = Preferences.userRoot().node(this.getClass().getName());
	private Preferences userPreferencesMainPage = Preferences.userRoot().node(UserLogin.class.getName());

	@FXML
	private Pane content;

	@FXML
	private TableView<Book> tableView;

	@FXML
	private TableColumn<Book, String> title;

	@FXML
	private TableColumn<Book, String> author;

	private User user;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		String email = userPreferencesMainPage.get("username", null);
		user = auth.getUser(email);


		List<Book> allBook = auth.allBooks();
		tableView.setItems(FXCollections.observableArrayList(allBook));
		settingColumnContents();
		tableView.setRowFactory(tv -> {
			TableRow<Book> row = settingRowColor();
			settingRowOnClick(row);
			return row;
		});

	}

	void settingRowOnClick(TableRow<Book> row) {
		row.setOnMouseClicked(event -> {
			Book book = row.getItem();
			Alert alert = dialog();
			alert.showAndWait();
			if (alert.getResult() == ButtonType.YES) {
				Rent rent = new Rent(book, new Date(), new Date());
				user.addRent(rent);

				em.getTransaction().begin();
				em.persist(user);
				em.getTransaction().commit();
				content.getChildren().clear();
				try {
					content.getChildren().add((Node) FXMLLoader.load(getClass().getResource("/view/BookList.fxml")));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				alert.close();
			}

		});
	}

	TableRow<Book> settingRowColor() {
		TableRow<Book> row = new TableRow<Book>() {
			@Override
			public void updateItem(Book item, boolean empty) {
				super.updateItem(item, empty);
			}
		};
		return row;
	}

	void settingColumnContents() {
		title.setCellValueFactory(new Callback<CellDataFeatures<Book, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Book, String> data) {
				return ObjectConstant.<String>valueOf(data.getValue().getTitle());
			}
		});

		author.setCellValueFactory(new Callback<CellDataFeatures<Book, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Book, String> data) {
				return ObjectConstant.<String>valueOf(data.getValue().getAuthor());
			}
		});

	}

	public Alert dialog() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "You are going to rent this book. Are you sure?",
				ButtonType.YES, ButtonType.NO);
		alert.initStyle(StageStyle.UNDECORATED);
		alert.setGraphic(null);
		DialogPane dialogPane = alert.getDialogPane();
		//dialogPane.getStylesheets().add(getClass().getResource("/view/appDialogs.css").toExternalForm());
		//dialogPane.getStyleClass().add("myDialog");
		return alert;
	}

}
