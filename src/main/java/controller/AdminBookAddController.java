package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import model.Book;
import org.controlsfx.control.Notifications;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.prefs.Preferences;

public class AdminBookAddController implements Initializable {

	private EntityManager entityManager = Context.getInstance().getEntityManager();

	@FXML
	private AnchorPane content;

	@FXML
	private TextField title;

	@FXML
	private TextField author;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));

	}

	@FXML
	private void save() {
		if (!title.getText().isEmpty() && !author.getText().isEmpty()) {
			Book book = new Book(title.getText(), author.getText());

			entityManager.getTransaction().begin();
			entityManager.persist(book);
			entityManager.getTransaction().commit();

			content.getChildren().clear();

			Notifications.create().title("Success").text("Book added").graphic(null)
					.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
			try {
				content.getChildren().add(FXMLLoader.load(getClass().getResource("/view/AdminBookList.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Notifications.create().title("Warning").text("Fill all the fileds").graphic(null)
					.hideAfter(Duration.seconds(3)).position(Pos.TOP_CENTER).show();
		}
	}
}
