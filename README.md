## Library Management System

The Library Management System is used to maintain records of transaction between
users and the library. This system contains two modules: Admin and User. Users can check their
personal data, view the available books, rent the desired books and receive updates of the due
dates, issue date of the books, and the penalty amount. Admin can add new books, modify and
remove the details of the books, can also register the users and check their inquiries and view
the due dates and the penalty amounts.

---

###Build

To build Library Management System, execute the following command:

`mvn clean install`

To compile the main demo, execute the following command:

`mvn clean compile exec:java`

---

###Run the Application

Please download the latest version from [here](https://sourceforge.net/projects/tsp-lms/files/library-management-system-1.0-SNAPSHOT.jar/download)

Already created credentials are given in the assignment submission. 

---

####Login

There are 2 login pages: one for user(e.g. student) one for admin(librarian).
There is a registration option for user. Admin will receive their credentials by default.


####User Page
User Page has 3 navigation buttons. 
First, dashboard which shows recent activities.
Second, books which show the available books in the library
Third, return which user can return their rented books. Here user can see the issue and due dates and details about the book.

####Admin Page
Admin Page includes library and the users options. Where admin can add, modify and delete the books and users. 

---

####Project members

Gasimov Huseyn

Meydan Alper

Purevdorj Usukhbayar

---

